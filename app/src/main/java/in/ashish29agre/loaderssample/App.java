package in.ashish29agre.loaderssample;

import android.graphics.drawable.Drawable;

/**
 * Created by ymedia on 20/7/15.
 */
public class App {
    private Drawable icon;
    private String label;

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
