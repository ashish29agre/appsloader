package in.ashish29agre.loaderssample;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;
import java.util.logging.Logger;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<List<App>> {

    private static final Logger log = Logger.getLogger(MainActivityFragment.class.getSimpleName());

    private ListView mAppsLv;

    private AppsAdapter mAdapter;

    public MainActivityFragment() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getActivity().getSupportLoaderManager().initLoader(121, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {

        log.info("Init views");

        mAppsLv = (ListView) view.findViewById(R.id.apps_lv);
    }

    @Override
    public Loader<List<App>> onCreateLoader(int id, Bundle args) {
        return new AppListLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<List<App>> loader, List<App> data) {
        mAdapter = new AppsAdapter(getActivity(), -1, data);
        mAppsLv.setAdapter(mAdapter);
        log.info("Loading finished :: ");
    }

    @Override
    public void onLoaderReset(Loader<List<App>> loader) {
        log.warning("Loader reset");
    }
}
