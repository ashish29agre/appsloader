package in.ashish29agre.loaderssample;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v4.content.AsyncTaskLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ymedia on 20/7/15.
 */
public class AppListLoader extends AsyncTaskLoader<List<App>> {

    private List<App> apps;
    private Context mContext;

    public AppListLoader(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();

        forceLoad();
    }

    @Override
    public List<App> loadInBackground() {

        final PackageManager pm = mContext.getPackageManager();
        int flags = PackageManager.GET_META_DATA | PackageManager.GET_SHARED_LIBRARY_FILES |
                PackageManager.GET_UNINSTALLED_PACKAGES | PackageManager.GET_DISABLED_COMPONENTS;
        //get a list of installed apps.
        List<ApplicationInfo> packages = pm.getInstalledApplications(flags);

        if (packages == null) {
            packages = new ArrayList<ApplicationInfo>();
        }

        // create corresponding apps and load their labels
        ArrayList<App> allPackages = new ArrayList<>();
        App packageItem;
        for (ApplicationInfo packageInfo : packages) {
            String pkg = packageInfo.packageName;

            if (pm.getLaunchIntentForPackage(pkg) != null) {
                packageItem = new App();
                packageItem.setIcon(packageInfo.loadIcon(pm));
                packageItem.setLabel(packageInfo.loadLabel(pm).toString());

                allPackages.add(packageItem);

            }
        }
        return allPackages;
    }


    @Override
    public void deliverResult(List<App> apps) {
        if (isReset()) {
            // An async query came in while the loader is stopped. We don't need the result
            if (apps != null) {
                onReleaseResources(apps);
            }
        }
        List<App> oldApps = this.apps;
        this.apps = apps;

        if (isStarted()) {
            // If the loader is currently started, we can immediately deliver a result
            super.deliverResult(apps);
        }

        // At this point we can release the resources associated with 'oldApps' if needed;
        // now that the new result is delivered we know that it is no longer in use
        if (oldApps != null) {
            onReleaseResources(oldApps);
        }
    }


    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    private void onReleaseResources(List<App> apps) {
        // For a simple list there is nothing to do
        // but for a Cursor we would close it here
    }
}
