package in.ashish29agre.loaderssample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ymedia on 20/7/15.
 */
public class AppsAdapter extends ArrayAdapter<App> {

    private List<App> apps;
    private LayoutInflater layoutInflater;

    public AppsAdapter(Context context, int resource, List<App> objects) {
        super(context, resource, objects);
        layoutInflater = LayoutInflater.from(context);
        this.apps = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        final App app = apps.get(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.app_item, parent, false);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.icon = (ImageView) convertView.findViewById(R.id.app_icon);
        viewHolder.label = (TextView) convertView.findViewById(R.id.app_label);

        viewHolder.icon.setImageDrawable(app.getIcon());
        viewHolder.label.setText("" + app.getLabel());

        return convertView;
    }

    private static final class ViewHolder {
        private TextView label;
        private ImageView icon;
    }
}
